FROM node:carbon
COPY . /app
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN npm install
EXPOSE 3000
RUN chmod +X startup.sh
ENTRYPOINT ["/app/startup.sh"]
#CMD ["npm","start"]
