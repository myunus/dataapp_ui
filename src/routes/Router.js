import React from 'react';
import DataValidationPage from '../components/DataValidation/DataValidationPage'
import AllJobs from '../components/DataValidation/AllJobs'
import HomePage from '../components/DataValidation/HomePage'
import DataPage from '../components/DataValidation/DataPage'
import { BrowserRouter ,Route, Switch} from 'react-router-dom';
import JobDetails from '../components/DataValidation/JobDetails';

const Router = () =>(
    <BrowserRouter>
        <div>
            <Switch>
                <Route path="/dataui" component={HomePage} exact={true} />
                <Route path="/dataui/data" component={DataPage} />
                <Route path="/dataui/athena-athena" component={DataValidationPage} />
                <Route path="/dataui/athena-mongo" component={DataValidationPage} />
                <Route path="/dataui/allJobs" component={AllJobs} />
                <Route path="/dataui/jobDetails/:id" component={JobDetails} />
            </Switch>
        </div>
    </BrowserRouter>
)

export default Router