import { createStore,combineReducers } from 'redux';
import dataValidationReducer from '../reducers/Data';
export default () => {
    const store = createStore(combineReducers({
        dataValidation : dataValidationReducer
    })
    );
    return store;
}


