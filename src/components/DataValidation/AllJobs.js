import React from 'react';
import regeneratorRuntime from 'regenerator-runtime';
import styles from '../../styles/styles.css';
//import Details from './Details';
import { NavLink} from 'react-router-dom';


export default class AllJobs extends React.Component{
    state = {
        finished_Jobs:[],
        failed_Jobs:[],
        started_Jobs:[],
        queued_Jobs:[],
        deleteAllFailedJobs:undefined,
        jobNo:''
    }
    async componentDidMount(){
        this.getAllJobs()
    }

    async getAllJobs(){
        const settings = {
            method: 'GET',
            headers: {
                //'x-api-key': '1kGmIVSLH43cadq6xlU6P4QSUQQyYq8A6QrrA2x7',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        };
        const url = 'http://localhost:5000/cqa/getAllJobs'
        const response = await fetch(url,settings)
        const data = await response.json();
        this.setState(()=>{
            return{
                finished_Jobs:data.Finished_Jobs,
                failed_Jobs:data.Failed_Jobs,
                started_Jobs:data.Started_Jobs
            }
        })
    }

    getStartedJobs = () =>{
        const started_jobs = this.state.started_Jobs
        const url = 'http://localhost:5000/cqa/getJobDetails/'
        return(
            <ul>
                {/*{started_jobs.map((job) => (<li key={job}><a href={url+job} >{job}</a></li>))}*/}
                {started_jobs.map((job)=> (<li key={job}><NavLink to={{pathname:'/dataui/jobDetails/'+job,state:{job}}}>{job}</NavLink></li>))}
            </ul>
        )
    }
    getFinishedJobs = () =>{
        const finished_jobs = this.state.finished_Jobs
        const url = 'http://localhost:5000/cqa/getJobDetails/'
        //<NavLink to="/jobDetails/{this.props.jobNo}" activeClassName="is-active" >{this.props.jobNo}</NavLink>
        
        return(
            <ul>
                {/*{finished_jobs.map((job) => (<li key={job}><a href={url+job} >{job}</a></li>))}*}
                {/*{finished_jobs.map((job) =>(<JobDetails key={job} jobNo={job}/>))}*/}
                {finished_jobs.map((job)=> (<li key={job}><NavLink to={{pathname:'/dataui/jobDetails/'+job,state:{job}}}>{job}</NavLink></li>))}
            </ul>
        )
    }

    getFailedJobs = () =>{
        const failed_jobs = this.state.failed_Jobs
        const url = 'http://localhost:5000/cqa/getJobDetails/'
        console.log(failed_jobs)
        return(
            <ul>
                {/*{failed_jobs.map((job) => (<li key={job}><a href={url+job}>{job}</a></li>))}*/}
                {/*{failed_jobs.map((job) =>(<JobDetails key={job} jobNo={job}/>))}*/}
                {/*{failed_jobs.map((job)=> (<li key={job}><Details job={job}/></li>))}*/}
                {failed_jobs.map((job)=> (<li key={job}><NavLink to={{pathname:'/dataui/jobDetails/'+job,state:{job}}}>{job}</NavLink></li>))}
            </ul>
        )
    }

    getQueuedJobs = () =>{
        const queued_jobs = this.state.queued_Jobs
        const url = 'http://localhost:5000/cqa/getJobDetails/'
        console.log(queued_jobs)
        return(
            <ul>
                {/*{failed_jobs.map((job) => (<li key={job}><a href={url+job}>{job}</a></li>))}*/}
                {/*{failed_jobs.map((job) =>(<JobDetails key={job} jobNo={job}/>))}*/}
                {/*{failed_jobs.map((job)=> (<li key={job}><Details job={job}/></li>))}*/}
                {queued_jobs.map((job)=> (<li key={job}><NavLink to={{pathname:'/jobDetails/'+job,state:{job}}}>{job}</NavLink></li>))}
            </ul>
        )
    }
    
    deleteAllFailedJobs = () =>{

        const url = 'http://localhost:5000/cqa/deleteAllFailedJobs'
        const settings = {
            method: 'GET',
            headers: {
                //'x-api-key': '1kGmIVSLH43cadq6xlU6P4QSUQQyYq8A6QrrA2x7',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };
        const data = fetch(url,settings).then((response) => response.json()).then((responseJSON) => {console.log(responseJSON.msg);this.setState(()=>{
            return{
                failed_Jobs: [],
            }
        }); responseJSON})
    }

   async getJobDetails(){
        console.log("hrer")
        const settings = {
            method: 'GET',
            headers: {
                //'x-api-key': '1kGmIVSLH43cadq6xlU6P4QSUQQyYq8A6QrrA2x7',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        };
        const url = 'http://localhost:5000/cqa/getJobDetails/87b1168c-42d2-4f30-92ce-b0343e3d5071'
        const response = await fetch(url,settings)
        const data = await response.json();
        //console.log(data)
        this.setState(()=>{
            return{
                jobDetails:data
            }
        })
    }
    render(){
        return(
            <div>
                Started Jobs
                {this.getStartedJobs()}
                Completed Jobs
                {/*<FinishedJobs finished_jobs = {this.state.finished_jobs}/>*/}
                {this.getFinishedJobs()}
                Failed Jobs <button className={styles.button} onClick={this.deleteAllFailedJobs}>Delete All</button>
                {this.getFailedJobs()}
               </div>
        )
    }


}
    

