import React from 'react';
import {NavLink} from 'react-router-dom'

const DataPage = () =>(
    
    <div>
    <div>
         <NavLink to="/dataui/athena-athena" activeClassName="is-active" exact={true}>Athena-Athena Validation</NavLink>
    </div>
    <div>
         <NavLink to="/dataui/athena-mongo" activeClassName="is-active">Athena-MongoDB Validation</NavLink>
    </div>
    </div>
    
)
 
export default DataPage