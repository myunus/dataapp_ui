import React from 'react';
import { NavLink} from 'react-router-dom'
const HomePage = () =>(
    <header>
    <h1>Data App</h1>
    <div>
         <NavLink to="/" activeClassName="is-active" exact={true}>Homepage</NavLink>
    </div>
    <div>
         <NavLink to="/dataui/data" activeClassName="is-active" >Data Validation</NavLink>
    </div>
    </header>
)

export default HomePage;