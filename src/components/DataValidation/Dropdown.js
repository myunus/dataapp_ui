import React from 'react';
import styles from '../../styles/styles.css'
import { render } from 'react-dom';
import { Multiselect } from 'multiselect-react-dropdown'
import { Redirect } from 'react-router-dom';


export default class Dropdown extends React.Component{

    css = {
        inputField:{
            margin:5,
            size:200,
        },
        searchBox: { // To change search box element look
            size: 100,
            height: 10,
          },
          chips: { // To change css chips(Selected options)
            fontSize:17,
            fontFamily:'Times New Roman',
            background: '#00BFA5',
            size:200
          },
          optionContainer: { // To change css for option container 
            border: '1px solid',
          },
          
         
    }
    render(){
        return(               
                <Multiselect
                    options={this.props.source}
                    displayValue="name"
                    singleSelect={this.props.singleSelect}
                    placeholder={this.props.placeholder}
                    hidePlaceholder={true}
                    style={this.css}
                    onSelect={this.props.selected}
                    selectionLimit={this.props.selectionLimit}
                /> 
        )

        
        
    }
    

   
    /*render(){
        return(
            <div className="divTop">
            <label className={styles.label}>Source :</label>
            <select className={styles.select} id ='source' onChange={this.props.sourceSelection}>
            {this.props.source.map((item)=>(<option key={item} className={styles.select} value={item}>{item}</option>))}
            </select>
        </div>
        )
    }*/

}


