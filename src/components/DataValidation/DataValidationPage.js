import React from 'react';
import regeneratorRuntime from 'regenerator-runtime';
import styles from '../../styles/styles.css';
import { NavLink} from 'react-router-dom';
import Dropdown from './Dropdown';



export default class DataValidationPage extends React.Component{
    
    state = {
        countJobId:undefined,comparisonJobId:undefined,conversionJobid:undefined,refJobId:undefined,mandatoryJobId:undefined,jobDetails:undefined,tenant:'',dealerShip:'',run:'',env:'',athenaDB:'',cdkDB:'',source:'',region:'',module:'',mongoDB:'',
        tenant_dealership_code:'',tenant_invoice_tax_perc:'',data_sets:'',dealer_company_number:'',dealer_account_number:'',tenant_upload_date:'',tapURL:'',
        sourceDMS:[{name:'CDK'},{name:'RandR'},{name:'DealerTrack'},{name:'Quorum'},{name:'DealerBuilt'},{name:'Dominion'}],
        sourceSelected:undefined,model_dbs:[],processed_dbs:[],modelDBSelected:'',processedDBSelected:''
    };


   
    handleTextFieldOnChange = (name) => event =>{
        if(name === 'module'){
            this.getDatabases(this.state.region)
        }
        
        this.setState({
          [name]: event.target.value,
        });
        //console.log(this.state)
        
      };
    getSelectedCheckBoxes(){
        //e.preventDefault()
        var checkedQueries = []; 
        var inputElements = document.getElementsByClassName('checkbox');
        console.log(inputElements.length)
        console.log(inputElements)
        for(let i=0; i<inputElements.length; i++){
            if(inputElements[i].checked){
                let id = inputElements[i].id
                console.log(id)
                checkedQueries.push(id)
            }
        }
        return checkedQueries
    }

    submitForm =(e) =>{
        e.preventDefault()
        console.log('hi')
        let inputs = {tenant:this.state.tenant,dealerShip:this.state.dealerShip,run:this.state.run,env:this.state.env,athenaDB:this.state.modelDBSelected,
            cdkDB:this.state.processedDBSelected,source:this.state.sourceSelected,region:this.state.region,module:this.state.module,mongoDB:this.state.mongoDB,
            tenant_dealership_code:this.state.tenant_dealership_code,tenant_invoice_tax_perc:this.state.tenant_invoice_tax_perc,data_sets:this.state.data_sets,
            dealer_company_number:this.state.dealer_company_number,dealer_account_number:this.state.dealer_account_number,tenant_upload_date:this.state.tenant_upload_date,tapURL:this.state.tapURL}
        console.log(inputs)
        let selectedQueries = this.getSelectedCheckBoxes(e)
        console.log(selectedQueries)
        const settings = {
            method: 'POST',
            body : JSON.stringify(inputs),
            headers: {
                //'x-api-key': '1kGmIVSLH43cadq6xlU6P4QSUQQyYq8A6QrrA2x7',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        };
        
        for (let selected of selectedQueries){
            console.log(selected)
            if (selected === 'count'){
                console.log(selected)
                const url = 'http://localhost:5000/cqa/count'
                const data = fetch(url,settings).then((response) => response.json()).then((responseJSON) => {this.setState(()=>{
                    return{
                        countJobId:responseJSON.job_id
                    }
                }); responseJSON})
            //console.log(this.state.countJobId)
            }else if(selected === 'comparison'){
                const url = 'http://localhost:5000/cqa/comparison'
                const data = fetch(url,settings).then((response) => response.json()).then((responseJSON) => {this.setState(()=>{
                    return{
                        comparisonJobId:responseJSON.job_id
                    }
                }); responseJSON})

            }else if(selected === 'ref_integrity'){
                const url = 'http://localhost:5000/cqa/ref_integrity'
                const data = fetch(url,settings).then((response) => response.json()).then((responseJSON) => {this.setState(()=>{
                    return{
                        refJobId:responseJSON.job_id
                    }
                }); responseJSON})

            }else if(selected === 'mandatory_fields'){
                const url = 'http://localhost:5000/cqa/mandatory_fields'
                const data = fetch(url,settings).then((response) => response.json()).then((responseJSON) => {this.setState(()=>{
                    return{
                        mandatoryJobId:responseJSON.job_id
                    }
                }); responseJSON})

            }else if(selected === 'conversion'){
                const url = 'http://localhost:5000/cqa/conversion'
                const data = fetch(url,settings).then((response) => response.json()).then((responseJSON) => {this.setState(()=>{
                    return{
                        conversionJobid:responseJSON.job_id
                    }
                }); responseJSON})

            }
            
        }
        //const response = await fetch(url,settings)
        //const data = await response.json();
        //console.log(this.state.countJobId)
        //console.log(data)
        //console.log(data)
        //alert(data)
        //fetch(url,settings).then((response) => response.json()).then((responseJSON) => {this.setState({jobNo:responseJSON.job_id})});
    }
    getJobNo = () =>{
        let job = document.getElementById('jobno').value
        this.getJobDetails(job)
    }
    async getJobDetails(jobNo){
        const settings = {
            method: 'GET',
            headers: {
                //'x-api-key': '1kGmIVSLH43cadq6xlU6P4QSUQQyYq8A6QrrA2x7',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        };
        const url = 'http://localhost:5000/cqa/getJobDetails/'+jobNo
        const response = await fetch(url,settings)
        const data = await response.json();
        //console.log(data)
        this.setState(()=>{
            return{
                jobDetails:data
            }
        })
    }
 

    printJobDetails = () =>{
        return JSON.stringify(this.state.jobDetails,null,4)
    }

    sourceSelected = (selectedList,source)=>{
        console.log(source)
        this.setState(()=>{
           return{
               sourceSelected:source
           }
       })
    }

    processedDBSelected = (selectedList,source)=>{
        console.log(source)
        this.setState(()=>{
            return{
                processedDBSelected:source
            }
        })
     }

    modelDBSelected = (selectedList,source) =>{
        console.log(source)
        this.setState(()=>{
            return{
                modelDBSelected:source
            }
        })
     }

    componentDidMount(){
        this.getDatabases()
    }
    getDatabases = (region='us-west-2') =>{
        const url = 'http://localhost:5000/cqa/db/getAthenaDBs'
        let model_inputs = {}
        let processed_inputs = {}
        if (region !== 'us-west-2'){
            model_inputs = {'env':'prod','region':this.state.region,'like':'models'}
            processed_inputs = {'env':'prod','region':this.state.region,'like':'processed'}
        }else{
            model_inputs = {'env':'prod','region':region,'like':'models'}
            processed_inputs = {'env':'prod','region':region,'like':'processed'}
        }
        
        const model_settings = {
            method: 'POST',
            body : JSON.stringify(model_inputs),
            headers: {
                //'x-api-key': '1kGmIVSLH43cadq6xlU6P4QSUQQyYq8A6QrrA2x7',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        };
        {/*}
        const model_data = fetch(url,model_settings).then((response) => response.json()).then((responseJSON) => {console.log("yunus");this.setState(()=>{
            return{
                model_dbs:responseJSON.databases
            }
        }); responseJSON})
    */}
     
    const model_data = fetch(url,model_settings).then((response) => response.json()).then((responseJSON) => {this.makeDictFromList(responseJSON,"models")})

        const processed_settings = {
            method: 'POST',
            body : JSON.stringify(processed_inputs),
            headers: {
                //'x-api-key': '1kGmIVSLH43cadq6xlU6P4QSUQQyYq8A6QrrA2x7',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        };
        const processed_data = fetch(url,processed_settings)
        .then((response) => response.json())
        .then((responseJSON) => {this.makeDictFromList(responseJSON,"processed")})
        .catch((error)=>{
            console.log(error)
        })

    }

    makeDictFromList(responseJSON,type){
        let dbs = responseJSON.databases
        let databases=[]

        if (Array.isArray(dbs) === true) {
            //dbs.map(db=>(databases.push(db_dropdown['name']=db)));
            for (let i =0;i< dbs.length;i++){
                let db_dropdown = {}
                //db_dropdown['name']=dbs[i]
                db_dropdown.name=dbs[i]
                databases.push(db_dropdown)
            }
            console.log(databases)
            if(type === 'models'){
                this.setState(()=>{
                    return{
                        model_dbs:databases
                    }
                })
            }
            if(type === 'processed'){
                this.setState(()=>{
                    return{
                        processed_dbs:databases
                    }
                })
            }
        }else{
            console.log("No databases returned") 
        }
    }
    render(){
        return(
            <div className="container">
                <form onSubmit={this.submitForm}>
                <div className="divDataInputs">
                    <label className={styles.label}>Tenant :</label><input type='text' id='tenant' value = {this.state.tenant} onChange={this.handleTextFieldOnChange('tenant')}/>
                    <label className={styles.label}>Dealership :</label><input type='text' id='dealership' value ={this.state.dealerShip} onChange={this.handleTextFieldOnChange('dealerShip')}/>
                    <label className={styles.label}>Run :</label><input type='text' id='run' value={this.state.run} onChange={this.handleTextFieldOnChange('run')}/>
                    <label className={styles.label}>Env :</label><input type='text' id='env' value={this.state.env} onChange={this.handleTextFieldOnChange('env')}/>
                    
                    
                </div>
                <div className="divDataInputs">
                    <label className={styles.label}>Region :</label><input type='text' id='region' value={this.state.region} onChange={this.handleTextFieldOnChange('region')}/>
                    <label className={styles.label}>Module :</label><input type='text' id='module' value={this.state.module} onChange={this.handleTextFieldOnChange('module')}/>
                    <label className={styles.label}>Data Sets :</label><input type='text' id='data_sets' value={this.state.data_sets} onChange={this.handleTextFieldOnChange('data_sets')}/>
                    <label className={styles.label}>Dealer Company Number :</label><input type='text' id='dealer_company_number' value={this.state.dealer_company_number} onChange={this.handleTextFieldOnChange('dealer_company_number')}/>
                    
                </div>
                <div className="divDataInputs">
                    <label className={styles.label}>Tenant Dealership Code :</label><input type='text' id='tenant_dealership_code' value={this.state.tenant_dealership_code} onChange={this.handleTextFieldOnChange('tenant_dealership_code')}/>
                    <label className={styles.label}>Tenant Invoice Tax % :</label><input type='text' id='tenant_invoice_tax_perc' value={this.state.tenant_invoice_tax_perc} onChange={this.handleTextFieldOnChange('tenant_invoice_tax_perc')}/>
                    <label className={styles.label}>Dealer Account Number :</label><input type='text' id='dealer_account_number' value={this.state.dealer_account_number} onChange={this.handleTextFieldOnChange('dealer_account_number')}/>
                    <label className={styles.label}>Tenant Upload Date :</label><input type='text' id='tenant_upload_date' value={this.state.tenant_upload_date} onChange={this.handleTextFieldOnChange('tenant_upload_date')}/>
                </div>
                <div className="divDataInputs">
                    {/*<label className={styles.label}>Tekion Model :</label><input type='text' id='athenadb' value={this.state.athenaDB} onChange={this.handleTextFieldOnChange('athenaDB')}/>
                    <label className={styles.label}>Source DMS Extract :</label><input type='text' id='cdkdb' value={this.state.cdkDB} onChange={this.handleTextFieldOnChange('cdkDB')}/>*/}
                    <label className={styles.label}>Source :</label>
                    <div className="divDropDown">
                    <Dropdown placeholder="Source" singleSelect={false} selectionLimit={1} source={this.state.sourceDMS} selected={this.sourceSelected}/>
                    </div>
                    <label className={styles.label}>Source DMS Extract :</label>
                    <div className="divDropDown">
                    <Dropdown placeholder="Processed DB's" singleSelect={false} selectionLimit={1} source={this.state.processed_dbs} selected={this.processedDBSelected}/>
                    </div>
                    
                    
                   {/*<label className={styles.label}>Source :</label><input type='text' id='source' className='text' value={this.state.source} onChange={this.handleTextFieldOnChange('source')}/>*/}
                </div>
                <div className="divDataInputs">
                <label className={styles.label}>Tekion Model :</label>
                    <div className="divDropDown">
                    <Dropdown placeholder="Model DB's" singleSelect={false} selectionLimit={1} source={this.state.model_dbs} selected={this.modelDBSelected}/>
                    </div>
                    <label className={styles.label}>Mongo DB :</label><input type='text' id='mongodb' value={this.state.mongoDB} onChange={this.handleTextFieldOnChange('mongoDB')}/>
                </div>
                <div className="divDataInputs center">
                    <label className={styles.label}>Count</label><input type='checkbox' id='count' className='checkbox'/>
                    <label className={styles.label}>Comparison</label><input type='checkbox' id='comparison' className='checkbox'/>
                    <label className={styles.label}>Ref Integrity</label><input type='checkbox' id='ref_integrity' className='checkbox'/>
                    <label className={styles.label}>Mandatory Fields</label><input type='checkbox' id='mandatory_fields' className='checkbox'/>
                    <label className={styles.label}>Conversion</label><input type='checkbox' id='conversion' className='checkbox'/>
                </div>
                <div className="divDataInputs">
                    <div className="center">
                        <button className="button">Submit</button>
                    </div>
                </div>
                </form>
                <div className="divDataInputs">
                    {/*{this.state.countJobId && <p><label className={styles.label}>Count</label><input type='text' id='count_jodid' className='text' value={this.state.countJobId} size='40' readOnly/></p>}
                    {this.state.comparisonJobId && <p><label className={styles.label}>Comparison</label><input type='text' id='comparison_jodid' className='text' value={this.state.comparisonJobId} size='40' readOnly/></p>}
                    {this.state.refJobId && <p><label className={styles.label}>Ref Integrity</label><input type='text' id='ref_jodid' className='text' value={this.state.refJobId} size='40' readOnly/></p>}
                    {this.state.mandatoryJobId && <p><label className={styles.label}>Mandatory Fields</label><input type='text' id='mandatory_jodid' className='text' value={this.state.mandatoryJobId} size='40' readOnly/></p>}
                    {this.state.conversionJobid && <p><label className={styles.label}>Conversion</label><input type='text' id='conversion_jobid' className='text' value={this.state.conversionJobid} size='40' readOnly/></p>}*/}
                    {(this.state.countJobId || this.state.comparisonJobId || this.state.refJobId || this.state.mandatoryJobId || this.state.conversionJobid) && <p><label><b>Submitted Jobs</b></label></p>}
                    {this.state.countJobId && <p>{this.state.countJobId}</p>}
                    {this.state.comparisonJobId && <p>{this.state.comparisonJobId}</p>}
                    {this.state.refJobId && <p>{this.state.refJobId}</p>}
                    {this.state.mandatoryJobId && <p>{this.state.mandatoryJobId}</p>}
                    {this.state.conversionJobid && <p>{this.state.conversionJobid}</p>}
                </div>
                <div className="divDataInputs">
                        <p><NavLink to="/dataui/allJobs" activeClassName="is-active" >Get All Jobs</NavLink></p>
                </div>
                <div>
                <label>Job No : </label><input size='35' type='text' id='jobno'/>
                <button onClick={this.getJobNo}>Get Job Details</button>
                {this.state.jobDetails && <p><textarea className="textarea" value={this.printJobDetails()} rows="100" cols="50" readOnly/></p>}
                </div>
            </div>   
        )
    }
}