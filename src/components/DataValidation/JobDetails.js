import React from 'react';
import regeneratorRuntime from 'regenerator-runtime';
import {useState} from 'react';
import styles from '../../styles/styles.css';
import { NavLink} from 'react-router-dom';
import { render } from 'react-dom';

export default class JobDetails extends React.Component{

    state = {
        jobDetails:''
    }

    printJobDetails = () =>{
        return JSON.stringify(this.state.jobDetails,null,4)
    }

    componentDidMount(){
        const a = this.props.match.params.id
        //console.log(a)
        const jobNo = this.props.location.state.job
        this.getJobDetails(jobNo)
    }
    async getJobDetails(jobNo){
        console.log(jobNo)
        const settings = {
            method: 'GET',
            headers: {
                //'x-api-key': '1kGmIVSLH43cadq6xlU6P4QSUQQyYq8A6QrrA2x7',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        };
        const url = 'http://localhost:5000/cqa/getJobDetails/'+jobNo
        const response = await fetch(url,settings)
        const data = await response.json();
        console.log(data)
        this.setState(()=>{
            return{
                jobDetails:data
            }
        })
    }

    render(){
        //console.log(this.props.location.state)
        return (
            <div>
                <p><textarea className={styles.textarea} value={this.printJobDetails()} rows="100" cols="50" readOnly/></p>
            </div>
        )
    }
}