import React from 'react';
import Modal from 'react-modal';
const JobModal = (props)=>(
    <Modal
        isOpen={!!props.selectedOption}
        contentLabel="Selected Option"
        closeTimeoutMS={200}
        >
        <h3>Selected Option</h3>
        {props.selectedOption && <p>{props.selectedOption}</p>}
        <button onClick={props.handleDeleteSelectedOption}>Okay</button>
    </Modal>
);

export default JobModal