
const array1 = ['mohammed','anis','humaira','ziyad']
for(let a in array1){
    console.log(a,array1[a])
}
for(let a of array1){
    console.log(a)
}

function max (a,b){
    if (a === b) {
        return 'both are equal'
    }else if(a > b){
        return a
    }else{
        return b
    }
}

const a = max(12,12)
console.log(a)

let circle = {
    radius : 1,
    location : {
        x : 1,
        y :2
    },
    area:function(){
        let a = 3.14 * (this.radius * this.radius)
        console.log(a)
    }
}
circle.radius = 5
circle.area();

function circle_new(radius,location){
    const a = {
        radius : radius,
        location : location,
        area(){
            return(3.14*radius*radius)
        }
    }
    return a
}

const v = circle_new(2,{x:2,y:3})
console.log(v.area())
console.log(v.location.x)

let array2 = [2,3,6,19,40]

let found = array2.find(function(a){
    return a > 10
})
console.log(found)

let found1 = array2.find((a) => {return a > 20});
console.log(found1)

console.log(array2[0])
while (array2.length > 0){
    console.log(array2.pop())
}
//console.log(array2)

//array generation

function arrayGeneration(min,max,step){
    let new_array = []
    for (let a = min;a <= max;a+=step){
        new_array.push(a)
    }
    return new_array
}
console.log(arrayGeneration(-20,5,2))

const array3 = arrayGeneration(1,10,1)
// search for the element in array 

function includes(array3,element){
    for(let i of array3){
        if (i === element){
            console.log("it exists")
        }
    }
}
includes(array3,10)

const name = {
    "first" : "mohammed",
    "last" : "yunus",
    get fullname(){
        return `${name.first} ${name.last}`
    },
    set fullname(value1){
        this.first = "yunus",
        this.last = "mohammed"
    }
}
name.fullname = 1;
console.log(name.fullname)