import React from 'react';
import { NavLink} from 'react-router-dom';
const Details = (props) =>{
    console.log(props.job)
    return(
        <div>
        <li key={props.job}><NavLink to={{pathname:"/jobDetails",state:{jobNo:props.job}}}>{props.job}</NavLink></li>
        </div>
    )
}
export default Details