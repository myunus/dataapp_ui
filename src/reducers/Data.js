const dataValidationDefaultState =  {
    jobNo:'',
}
export default dataValidationReducer = (state=dataValidationDefaultState,action) =>{
    switch(action.type){
        case 'SET_COUNT_JOB':
        return {
            jobNo:action.jobNo
        }
        default:
            return state
    }
};